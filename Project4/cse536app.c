/*
	Work with character device driver
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <linux/kernel.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>

#define NTP_SHARE
#define SERVER_PORT 23456 
#define MAX_PENDING 5
#define MAX_LINE 256
#define NTP_TIMESTAMP_DELTA 2208988800ull

inline void error( char* msg )
{
    perror( msg ); // Print the error message to stderr.
    exit( 0 ); // Quit the process.
}

struct packetformat{

uint32_t record_id;

uint32_t final_clock;

uint32_t original_clock;

__be32 source_ip;

__be32 destination_ip;

uint8_t data[236];

time_t txTm;

}datf, tp, wp;

// Structure that defines the 48 byte NTP packet protocol.
typedef struct 
{
	unsigned li   : 2;       	// Only two bits. Leap indicator.
	unsigned vn   : 3;       	// Only three bits. Version number of the protocol.
	unsigned mode : 3;       	// Only three bits. Mode. Client will pick mode 3 for client.
	
	uint8_t stratum;         	// Eight bits. Stratum level of the local clock.
	uint8_t poll;            	// Eight bits. Maximum interval between successive messages.
	uint8_t precision;       	// Eight bits. Precision of the local clock.
	
	uint32_t rootDelay;      	// 32 bits. Total round trip delay time.
	uint32_t rootDispersion; 	// 32 bits. Max error aloud from primary clock source.
	uint32_t refId;          	// 32 bits. Reference clock identifier.
	
	uint32_t refTm_s;        	// 32 bits. Reference time-stamp seconds.
	uint32_t refTm_f;        	// 32 bits. Reference time-stamp fraction of a second.
	
	uint32_t origTm_s;       	// 32 bits. Originate time-stamp seconds.
	uint32_t origTm_f;       	// 32 bits. Originate time-stamp fraction of a second.
	
	uint32_t rxTm_s;         	// 32 bits. Received time-stamp seconds.
	uint32_t rxTm_f;         	// 32 bits. Received time-stamp fraction of a second.
	
	uint32_t txTm_s;         	// 32 bits and the most important field the client cares about. Transmit time-stamp seconds.
	uint32_t txTm_f;         	// 32 bits. Transmit time-stamp fraction of a second.	
} ntp_packet;           		// Total: 384 bits or 48 bytes.


time_t getNTP(void)
{
	int ret, sockfd, n; // Socket file descriptor and the n return result from writing/reading from the socket.
	int portno = 123; // NTP UDP port number.
	char* host_name = "us.pool.ntp.org"; // NTP server host-name.
	
	// Create and zero out the packet. All 48 bytes worth.
	ntp_packet packet = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	
	memset( &packet, 0, sizeof( ntp_packet ) );
	
	// Set the first byte's bits to 00,011,011 for li = 0, vn = 3, and mode = 3. The rest will be left set to zero.
	
	*( ( char * ) &packet + 0 ) = 0x1b; // Represents 27 in base 10 or 00011011 in base 2.
	
	// Create a UDP socket, convert the host-name to an IP address, set the port number,
	// connect to the server, send the packet, and then read in the return packet.

	struct sockaddr_in serv_addr; // Server address data structure.
	struct hostent *server;	     // Server data structure.
	
	sockfd = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP ); // Create a UDP socket.
	
	if ( sockfd < 0 ) 
		error( "ERROR opening socket" );
	
	server = gethostbyname( host_name ); // Convert URL to IP.
	
	if ( server == NULL ) 
		error( "ERROR, no such host" );
	
	// Zero out the server address structure.
	
	bzero( ( char* ) &serv_addr, sizeof( serv_addr ) );
	
	serv_addr.sin_family = AF_INET;
	
	// Copy the server's IP address to the server address structure.
	
	bcopy( ( char* )server->h_addr, ( char* ) &serv_addr.sin_addr.s_addr, server->h_length );
	
	// Convert the port number integer to network big-endian style and save it to the server address structure.
	
	serv_addr.sin_port = htons( portno );
	
	// Call up the server using its IP address and port number.
	
	if ( connect( sockfd, ( struct sockaddr * ) &serv_addr, sizeof( serv_addr) ) < 0 ) 
		error( "ERROR connecting" );
	
	// Send it the NTP packet it wants. If n == -1, it failed.

	n = write( sockfd, ( char* ) &packet, sizeof( ntp_packet ) );
	
	if ( n < 0 ) 
		error( "ERROR writing to socket" );
	
	// Wait and receive the packet back from the server. If n == -1, it failed.
	
	n = read( sockfd, ( char* ) &packet, sizeof( ntp_packet ) );
	
	if ( n < 0 ) 
		error( "ERROR reading from socket" );
	
	// These two fields contain the time-stamp seconds as the packet left the NTP server.
	// The number of seconds correspond to the seconds passed since 1900.
	// ntohl() converts the bit/byte order from the network's to host's "endianness".

	packet.txTm_s = ntohl( packet.txTm_s ); // Time-stamp seconds.
	packet.txTm_f = ntohl( packet.txTm_f ); // Time-stamp fraction of a second.
	
	// Extract the 32 bits that represent the time-stamp seconds (since NTP epoch) from when the packet left the server.
	// Subtract 70 years worth of seconds from the seconds since 1900.
	// This leaves the seconds since the UNIX epoch of 1970.
	// (1900)------------------(1970)**************************************(Time Packet Left the Server)
	
	time_t txTm = ( time_t ) ( packet.txTm_s - NTP_TIMESTAMP_DELTA ); 
	
	// Print the time we got from the server, accounting for local timezone and conversion from UTC time.
		
	printf( "Time: %s", ctime( ( const time_t* ) &txTm ) );

	// This requires superuser permissions
	ret = stime(&txTm);
	if(ret == -1)
		printf("Set System Time failed! - error no: %d\n", errno);
	
	return txTm;
}

FILE *openfile(char *opts)
{
	FILE *fd = NULL;
	fd = fopen("/dev/cse5361", opts);
	if (!fd)
	{	printf("File error opening file\n");
	}
	return fd;
}

void sentom(char* data, char *monitor_ip)
{

//printf("ack send to monitor\n");
struct sockaddr_in client, server;
   struct hostent *hp;
   char buf[MAX_LINE];
   int len, ret, n;
   int s, new_s;

   bzero((char *)&server, sizeof(server));
   server.sin_family = AF_INET;
   server.sin_addr.s_addr = INADDR_ANY;
   server.sin_port = htons(0);

   s = socket(AF_INET, SOCK_DGRAM, 0);
   if (s < 0)
   {
		perror("simplex-talk: UDP_socket error");
		exit(1);
   }

   if ((bind(s, (struct sockaddr *)&server, sizeof(server))) < 0)
   {
		perror("simplex-talk: UDP_bind error");
		exit(1);
   }

   hp = gethostbyname( monitor_ip );
   if( !hp )
   {
      	fprintf(stderr, "Unknown host %s\n", "localhost");
      	exit(1);
   }

   bzero( (char *)&server, sizeof(server));
   server.sin_family = AF_INET;
   bcopy( hp->h_addr, (char *)&server.sin_addr, hp->h_length );
   server.sin_port = htons(SERVER_PORT);
   ret = sendto(s, data, 256, 0,(struct sockaddr *)&server, sizeof(server));
   if( ret <= 0)
   {
	fprintf( stderr, "Datagram Send error %d\n", ret );
   }
}

int main(int argc, char *argv[])
{
	FILE *fd = NULL;
	char buffer[257], monitor_ip[30] = {"192.168.0.39"}, input, addr[30] = {"192.168.0.40"};
	size_t count;
	int quit = 0, ch;
	uint32_t tclock;
	while(quit == 0){

		printf("\n********Main Menu********\n");
		printf("  (D) Set destination address\n");
		printf("  (M) Set monitor address\n");
		printf("  (W) Write to the device\n");
		printf("  (R) Read from the device\n");
		printf("  (Q) Quit\n");
		printf("  *************************\n");
		printf("  Option: ");

		scanf("%c", &input);
		input = toupper(input);

		while( (ch = fgetc(stdin)) != EOF && ch != '\n' ){}	//clear stream

		switch(input)
		{
			case 'D':
				printf("Please input destination IP address (ex. 192.168.0.40):\n");
				scanf("%256s", addr);
				while( (ch = fgetc(stdin)) != EOF && ch != '\n' ){}	//clear stream
				memset(buffer, 0, 257);
				buffer[0] = 0;
				memcpy(buffer+1, addr, strlen(addr)+1);
				fd = openfile("wb");
				if (fd)
				{
					fwrite(buffer, 1, strlen(addr)+1, fd);
					printf("Destination address set to: %s\n", addr);
					fclose(fd);
				}
			break;
			case 'M':
				printf("Please input monitor IP address (ex. 192.168.0.39):\n");
				scanf("%30s",  monitor_ip);
				while( (ch = fgetc(stdin)) != EOF && ch != '\n' ){}	//clear stream
			break;
			case 'W':
				memset(&datf, 0, 256);
				printf("Please input string:\n");
				scanf("%236[^\n]", datf.data);		// exception to new line
				while( (ch = fgetc(stdin)) != EOF && ch != '\n' ){}	//clear stream

				datf.record_id = 1;
				datf.final_clock = 0;
				datf.original_clock = 0;
#ifdef NTP_SHARE
				datf.txTm = getNTP();
#else
				datf.txTm = 0;
#endif
				inet_aton("192.168.0.40", (struct in_addr *)&datf.source_ip);
				inet_aton(addr, (struct in_addr *)&datf.destination_ip);
				fd = openfile("wb");
				if (fd)
				{
					fwrite(&datf, 1, sizeof(datf), fd);
					printf("Message sent to destination PC: %s\n", datf.data);
					fclose(fd);
					memset(&wp, 0, 256);
                                        fd = openfile("rb"); 
                                        fread((char*)&wp, 1, sizeof(wp), fd);
                                        sentom((char *)&wp, monitor_ip);
					printf("Event sent to Monitor: %s\n", wp.data);
					fclose(fd);
				}

			break;
			case 'R':
				fd = openfile("rb");
				if (fd)
				{
					memset(&tp, 0, 256);
					tp.record_id =5;
					count = fread(&tp, 1, sizeof(tp)-1, fd);
					if (!count)
						printf("No data read\n");
					else if(tp.record_id == 0 /*&& tp.source_ip == datf.source_ip && tp.destination_ip == datf.destination_ip*/)
					{
						sentom((char*)&tp, monitor_ip);
						printf("Ack sent to Monitor: %s\n", tp.data);
					}else if(tp.record_id == 1)
					{
						printf("%s\n", tp.data);
					}
#ifndef NTP_SHARE
					if(tp.txTm != 0)
						stime(&tp.txTm);
#endif
					fclose(fd);
				}
			break;
			case 'Q':
				quit = 1;
			break;
			default :
				printf("Unknown Entry\n");
				break;
		}
	}
	return 0;
}
